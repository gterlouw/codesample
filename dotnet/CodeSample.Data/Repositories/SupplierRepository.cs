﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using CodeSample.Data.Domain;
using CodeSample.Data.Repositories.Interfaces;
using Dapper;
using log4net;

namespace CodeSample.Data.Repositories
{
    public class SupplierRepository : ISupplierRepository
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IEnumerable<Supplier> Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    var suppliers = connection.Query<Supplier>("SELECT SupplierID," +
                                                               "CompanyName, " +
                                                               "ContactName, " +
                                                               "ContactTitle, " +
                                                               "Address, " +
                                                               "City, " +
                                                               "Region, " +
                                                               "PostalCode, " +
                                                               "Country, " +
                                                               "Phone, " +
                                                               "Fax " +
                                                               "FROM Suppliers");
                    return suppliers;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public Supplier Get(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    var supplier = connection.Query<Supplier>("SELECT SupplierID as Id," +
                                                               "CompanyName as Name, " +
                                                               "ContactName, " +
                                                               "ContactTitle, " +
                                                               "Address, " +
                                                               "City, " +
                                                               "Region, " +
                                                               "PostalCode, " +
                                                               "Country, " +
                                                               "Phone, " +
                                                               "Fax " +
                                                               "FROM Suppliers " +
                                                               "WHERE SupplierID = @Id", new { id }).SingleOrDefault();
                    return supplier;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            };
        }

        public Supplier Create(Supplier supplier)
        {
            try
            {
                using (
                    var connection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    var supplierId = connection.Query<int>("INSERT INTO Suppliers " +
                                                           "(CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax) " +
                                                           "VALUES (@Name, @ContactName, @ContactTitle, @Address, @City, @Region, @PostalCode, @Country, @Phone, @Fax) " +
                                                           "SELECT CAST(SCOPE_IDENTITY() as int)",
                                       new { supplier.Name, supplier.ContactName, supplier.ContactTitle, supplier.Address, supplier.City, supplier.Region, supplier.PostalCode, supplier.Country, supplier.Phone, supplier.Fax })
                                       .SingleOrDefault();

                    return Get(supplierId);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public Supplier Update(Supplier supplier)
        {
            try
            {
                using (
                    var connection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    var supplierToUpdate = Get(supplier.Id);
                    if (supplierToUpdate == null)
                        throw new Exception(string.Format("No supplier with id {0} found.", supplier.Id));

                    connection.Execute("UPDATE Suppliers" +
                                       "SET " +
                                       "CompanyName = @Name, " +
                                       "ContactName = @ContactName, " +
                                       "ContactTitle = @ContactTitle, " +
                                       "Address = @Address, " +
                                       "City = @City, " +
                                       "Region = @Region, " +
                                       "PostalCode = @PostalCode, " +
                                       "Country = @Country, " +
                                       "Phone = @Phone, " +
                                       "Fax = @Fax " +
                                       "WHERE SupplierID = @Id",
                                       new { supplier.Name, supplier.ContactName, supplier.ContactTitle, supplier.Address, supplier.City, supplier.Region, supplier.PostalCode, supplier.Country, supplier.Phone, supplier.Fax, supplier.Id });

                    return Get(supplier.Id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public void Delete(int id)
        {
            try
            {
                using (
                       var connection =
                           new SqlConnection(
                               ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    connection.Execute("DELETE FROM Suppliers WHERE SupplierID = @Id", new { id });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
            }
        }
    }
}