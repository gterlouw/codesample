﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using CodeSample.Data.Domain;
using CodeSample.Data.Repositories.Interfaces;
using Dapper;
using log4net;

namespace CodeSample.Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IEnumerable<Product> Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();

                    var products = connection.Query<Product>("SELECT " +
                                                             "ProductID AS Id, " +
                                                             "ProductName AS Name, " +
                                                             "SupplierId, " +
                                                             "CategoryId, " +
                                                             "QuantityPerUnit, " +
                                                             "UnitPrice, " +
                                                             "UnitsInStock, " +
                                                             "UnitsOnOrder, " +
                                                             "ReorderLevel, " +
                                                             "Discontinued " +
                                                             "FROM Products");
                    return products;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public Product Get(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    var product = connection.Query<Product>("SELECT " +
                                                             "ProductID AS Id, " +
                                                             "ProductName AS Name, " +
                                                             "SupplierId, " +
                                                             "CategoryId, " +
                                                             "QuantityPerUnit, " +
                                                             "UnitPrice, " +
                                                             "UnitsInStock, " +
                                                             "UnitsOnOrder, " +
                                                             "ReorderLevel, " +
                                                             "Discontinued " +
                                                             "FROM Products " +
                                                             "WHERE ProductID = @Id", new { id }).SingleOrDefault();
                    return product;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }

        }

        public Product Create(Product product)
        {
            try
            {
                using (
                    var connection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    var productId = connection.Query<int>(
                        "INSERT INTO Products (ProductName, SupplierID, CategoryID, QuantityPerUnit ,UnitPrice, UnitsInStock, UnitsOnOrder, ReorderLevel, Discontinued) " +
                        "VALUES (@Name, @SupplierId, @CategoryId, @QuantityPerUnit, @UnitPrice, @UnitsInStock, @UnitsOnOrder, @ReorderLevel, @Discontinued) " +
                        "SELECT CAST(SCOPE_IDENTITY() as int)",
                                       new
                                       {
                                           product.Name,
                                           product.SupplierId,
                                           product.CategoryId,
                                           product.QuantityPerUnit,
                                           product.UnitPrice,
                                           product.UnitsInStock,
                                           product.UnitsOnOrder,
                                           product.ReorderLevel,
                                           product.Discontinued
                                       }).SingleOrDefault();

                    return Get(productId);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public Product Update(Product product)
        {
            try
            {
                using (
                    var connection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();

                    var productToUpdate = Get(product.Id);
                    if(productToUpdate == null)
                        throw new Exception(string.Format("No product with id {0} found.", product.Id));

                    connection.Execute("UPDATE Products" +
                                       "SET " +
                                       "ProductName = @Name, " +
                                       "SupplierId = @SupplierId, " +
                                       "CategoryId = @CategoryId, " +
                                       "QuantityPerUnit = @QuantityPerUnit, " +
                                       "UnitPrice = @UnitPrice, " +
                                       "UnitsInStock = @UnitsInStock, " +
                                       "UnitsOnOrder = @UnitsOnOrder, " +
                                       "ReorderLevel = @ReorderLevel, " +
                                       "Discontinued = @Discontinued " +
                                       "FROM Products " +
                                       "WHERE ProductID = @Id",
                                       new
                                       {
                                           product.Name,
                                           product.SupplierId,
                                           product.CategoryId,
                                           product.QuantityPerUnit,
                                           product.UnitPrice,
                                           product.UnitsInStock,
                                           product.UnitsOnOrder,
                                           product.ReorderLevel,
                                           product.Discontinued,
                                           product.Id
                                       });

                    return Get(product.Id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public void Delete(int id)
        {
            try
            {
                using (
                    var connection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    connection.Execute("DELETE FROM Products WHERE ProductID = @Id", new { id });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
            }
        }
    }
}