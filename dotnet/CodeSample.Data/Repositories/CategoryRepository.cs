﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using CodeSample.Data.Domain;
using CodeSample.Data.Repositories.Interfaces;
using Dapper;
using log4net;
using log4net.Repository.Hierarchy;

namespace CodeSample.Data.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IEnumerable<Category> Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();

                    var categories = connection.Query<Category>("SELECT " +
                                                             "CategoryID AS Id, " +
                                                             "CategoryName AS Name, " +
                                                             "Description " +
                                                             "FROM Categories");
                    return categories;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public Category Get(int id)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();

                    var category = connection.Query<Category>("SELECT " +
                                                             "CategoryID AS Id, " +
                                                             "CategoryName AS Name, " +
                                                             "Description " +
                                                             "FROM Categories " +
                                                             "WHERE CategoryID = @Id", new { id }).SingleOrDefault();
                    return category;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public Category Create(Category category)
        {
            try
            {
                using (
                    var connection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    var categoryId = connection.Query<int>("INSERT INTO Categories (CategoryName, Description) " +
                                                           "VALUES (@Name, @Description)" +
                                                           "SELECT CAST(SCOPE_IDENTITY() as int)",
                                                           new { category.Name, category.Description }).SingleOrDefault();

                    return Get(categoryId);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public Category Update(Category category)
        {
            try
            {
                using (
                    var connection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {

                    var categoryToUpdate = Get(category.Id);
                    if (categoryToUpdate == null)
                        throw new Exception(string.Format("No category with id {0} found.", category.Id));

                    connection.Execute("UPDATE Categories SET " +
                                        "CategoryName = @Name, " +
                                        "Description = @Description " +
                                        "WHERE CategoryID = @Id",
                                        new
                                        {
                                            category.Name,
                                            category.Description,
                                            category.Id
                                        });

                    return Get(category.Id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        public void Delete(int id)
        {
            try
            {
                using (
                       var connection =
                           new SqlConnection(
                               ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString))
                {
                    connection.Open();
                    connection.Execute("DELETE FROM Categories WHERE CategoryID = @Id", new { id });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
            }
        }
    }
}