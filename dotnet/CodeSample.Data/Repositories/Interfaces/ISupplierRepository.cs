﻿using System.Collections.Generic;
using CodeSample.Data.Domain;

namespace CodeSample.Data.Repositories.Interfaces
{
    public interface ISupplierRepository
    {
        IEnumerable<Supplier> Get();
        Supplier Get(int id);
        Supplier Update(Supplier supplier);
        void Delete(int id);

        Supplier Create(Supplier supplier);
    }
}