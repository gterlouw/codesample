﻿using System.Collections.Generic;
using CodeSample.Data.Domain;

namespace CodeSample.Data.Repositories.Interfaces
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> Get();
        Category Get(int id);
        Category Create(Category category);
        Category Update(Category category);
        void Delete(int id);
    }
}