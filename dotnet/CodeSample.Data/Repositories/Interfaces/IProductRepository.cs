﻿using System.Collections.Generic;
using CodeSample.Data.Domain;

namespace CodeSample.Data.Repositories.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<Product> Get();
        Product Get(int id);
        Product Create(Product product);
        Product Update(Product product);
        void Delete(int id);
    }
}