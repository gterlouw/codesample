﻿namespace CodeSample.Business.Services.Interfaces
{
    public interface IEmailService
    {
        void Send(string subject, string body, string[] receivers);
        void Send(string subject, string body, string receiver);
    }
}