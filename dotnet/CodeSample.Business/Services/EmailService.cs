﻿using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Web;
using CodeSample.Business.Services.Interfaces;

namespace CodeSample.Business.Services
{
    public class EmailService : IEmailService
    {
        private static string From => ConfigurationManager.AppSettings["FromEmailaddress"];

        public void Send(string subject, string body, string[] receivers)
        {
            using (var message = new MailMessage())
            {
                foreach (var receiver in receivers)
                    message.Bcc.Add(receiver);
                message.From = new MailAddress(From);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;
                message.SubjectEncoding = Encoding.UTF8;

                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Send(message);
                }
            }
        }

        public void Send(string subject, string body, string receiver)
        {
            using (var message = new MailMessage(From, receiver))
            {
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;
                message.SubjectEncoding = Encoding.UTF8;

                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Send(message);
                }
            }
        }
    }   
}