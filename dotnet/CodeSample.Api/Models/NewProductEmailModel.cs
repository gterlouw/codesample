﻿namespace CodeSample.Api.Models
{
    public class NewProductEmailModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Url { get; set; }
    }
}