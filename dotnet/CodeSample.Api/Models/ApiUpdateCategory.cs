﻿using System.ComponentModel.DataAnnotations;

namespace CodeSample.Api.Models
{
    public class ApiUpdateCategory
    {
        [Required]
        public int Id { get; set; }
        [StringLength(15)]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}