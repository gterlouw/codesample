﻿using System.ComponentModel.DataAnnotations;

namespace CodeSample.Api.Models
{
    public class ApiCreateCategory
    {
        [StringLength(15)]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}