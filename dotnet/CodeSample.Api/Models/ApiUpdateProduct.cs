﻿using System.ComponentModel.DataAnnotations;

namespace CodeSample.Api.Models
{
    public class ApiUpdateProduct
    {
        [Required]
        public int Id { get; set; }
        [StringLength(40)]
        public string Name { get; set; }
        public int SupplierId { get; set; }
        public int CategoryId { get; set; }
        [StringLength(20)]
        public string QuantityPerUnit { get; set; }
        public double UnitPrice { get; set; }
        public int UnitsInStock { get; set; }
        public int UnitsOnOrder { get; set; }
        public int ReorderLevel { get; set; }
        public bool Discontinued { get; set; }  
    }
}