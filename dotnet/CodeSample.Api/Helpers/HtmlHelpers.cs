﻿using System;
using System.Web.Mvc;

namespace CodeSample.Api.Helpers
{
    public static class HtmlHelpers
    {
        public static string Link(this HtmlHelper html, string url, string text, string target)
        {
            return $"<a href=\"{url}\" target=\"{target}\">{text}</a>";
        }
    }
}