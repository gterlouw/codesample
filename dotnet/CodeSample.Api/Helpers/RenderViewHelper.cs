﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CodeSample.Api.Controllers;

namespace CodeSample.Api.Helpers
{
    public class RenderViewHelper
    {
        public static string RenderViewToString(string controllerName, string viewName, object viewData)
        {
            HttpContextBase contextBase = new HttpContextWrapper(HttpContext.Current);

            var routeData = new RouteData();
            routeData.Values.Add("controller", controllerName);
            var controllerContext = new ControllerContext(contextBase, routeData, new EmptyController());

            var razorViewEngine = new RazorViewEngine();
            var razorViewResult = razorViewEngine.FindView(controllerContext, viewName, "", false);

            var writer = new StringWriter();
            var viewContext = new ViewContext(controllerContext, razorViewResult.View, new ViewDataDictionary(viewData), new TempDataDictionary(), writer);
            razorViewResult.View.Render(viewContext, writer);

            return writer.ToString()
                .Replace(Environment.NewLine, string.Empty)
                .Replace("    ", string.Empty); // HACK to strip \r\n and spaces-per-4
        }
    }
}