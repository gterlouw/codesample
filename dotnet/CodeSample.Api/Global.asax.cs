﻿using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using log4net.Config;

namespace CodeSample.Api
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            // Initialize log4net.
            XmlConfigurator.Configure();
        }
    }
}
