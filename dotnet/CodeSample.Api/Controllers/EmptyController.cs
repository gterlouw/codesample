﻿using System.Web.Mvc;

namespace CodeSample.Api.Controllers
{
    public class EmptyController : ControllerBase
    {
        protected override void ExecuteCore() { }
    }
}