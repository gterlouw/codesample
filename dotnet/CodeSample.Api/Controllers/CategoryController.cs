﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CodeSample.Api.Models;
using CodeSample.Api.Security.Attributes;
using CodeSample.Data.Domain;
using CodeSample.Data.Repositories;
using CodeSample.Data.Repositories.Interfaces;

namespace CodeSample.Api.Controllers
{
    public class CategoryController : ApiController
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger
   (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ICategoryRepository _categoryRepository;

        public ICategoryRepository CategoryRepository
        {
            get { return _categoryRepository ?? (_categoryRepository = new CategoryRepository()); }
        }

        // GET api/category
        public IEnumerable<Category> Get()
        {
            try
            {
                return CategoryRepository.Get();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        // GET api/category/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var category = CategoryRepository.Get(id);
                if (category == null)
                    return NotFound();

                return Ok(category);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        // POST api/category
        [HttpPost]
        [Authorize]
        [WebApiAuthentication]
        public HttpResponseMessage Post([FromBody]ApiCreateCategory newCategory)
        {
            try
            {
                if (!ModelState.IsValid || newCategory == null)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                var category = CategoryRepository.Create(new Category
                {
                    Name = newCategory.Name,
                    Description = newCategory.Description
                });

                return Request.CreateResponse(HttpStatusCode.Created, category);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
            }
        }

        // PUT api/category/5
        [HttpPut]
        [Authorize]
        [WebApiAuthentication]
        public HttpResponseMessage Put([FromBody]ApiUpdateCategory updatedCategory)
        {
            try
            {
                if (!ModelState.IsValid || updatedCategory == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                var categoryToUpdate = CategoryRepository.Get(updatedCategory.Id);
                if (categoryToUpdate == null)
                    throw new NullReferenceException(string.Format("No category with id {0} available.", updatedCategory.Id));

                var category = CategoryRepository.Update(
                    new Category
                    {
                        Id = updatedCategory.Id,
                        Name = updatedCategory.Name,
                        Description = updatedCategory.Description
                    });

                return Request.CreateResponse(HttpStatusCode.OK, category);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
            }
        }

        // DELETE api/category/5
        [Authorize]
        [WebApiAuthentication]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (id == 0)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                var categoryToDelete = CategoryRepository.Get(id);
                if (categoryToDelete == null)
                    throw new NullReferenceException(string.Format("No category with id {0} available.", id));

                CategoryRepository.Delete(id);
                return Request.CreateResponse(HttpStatusCode.OK, string.Format("Category with id {0} deleted.", id));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
            }
        }
    }
}