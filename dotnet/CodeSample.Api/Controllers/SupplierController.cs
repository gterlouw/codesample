﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CodeSample.Api.Models;
using CodeSample.Api.Security.Attributes;
using CodeSample.Data.Domain;
using CodeSample.Data.Repositories;
using CodeSample.Data.Repositories.Interfaces;

namespace CodeSample.Api.Controllers
{
    public class SupplierController : ApiController
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger
   (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ISupplierRepository _supplierRepository;
        public ISupplierRepository SupplierRepository
        {
            get { return _supplierRepository ?? (_supplierRepository = new SupplierRepository()); }
        }

        // GET api/Supplier
        public IEnumerable<Supplier> Get()
        {
            try
            {
                return SupplierRepository.Get();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        // GET api/Supplier/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var supplier = SupplierRepository.Get(id);
                if (supplier == null)
                    return NotFound();

                return Ok(supplier);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        // POST api/Supplier
        [HttpPost]
        [Authorize]
        [WebApiAuthentication]
        public HttpResponseMessage Post([FromBody]ApiCreateSupplier newSupplier)
        {
            try
            {
                if (!ModelState.IsValid || newSupplier == null)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                if (ModelState.IsValid && newSupplier != null)
                {
                    var supplier = SupplierRepository.Create(new Supplier
                    {
                        Name = newSupplier.Name,
                        ContactName = newSupplier.ContactName,
                        ContactTitle = newSupplier.ContactTitle,
                        Address = newSupplier.Address,
                        City = newSupplier.City,
                        Region = newSupplier.Region,
                        PostalCode = newSupplier.PostalCode,
                        Country = newSupplier.Country,
                        Phone = newSupplier.Phone,
                        Fax = newSupplier.Fax
                    });

                    return Request.CreateResponse(HttpStatusCode.Created, supplier);
                }

                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
            }
        }

        // PUT api/Supplier/5
        [HttpPut]
        [Authorize]
        [WebApiAuthentication]
        public HttpResponseMessage Put([FromBody]ApiUpdateSupplier updatedSupplier)
        {
            try
            {
                if (!ModelState.IsValid || updatedSupplier == null)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                var supplierToUpdate = SupplierRepository.Get(updatedSupplier.Id);
                if (supplierToUpdate == null)
                    throw new NullReferenceException(string.Format("No Supplier with id {0} available.", updatedSupplier.Id));

                var supplier = SupplierRepository.Update(new Supplier
                {
                    Id = updatedSupplier.Id,
                    Name = updatedSupplier.Name,
                    ContactName = updatedSupplier.ContactName,
                    ContactTitle = updatedSupplier.ContactTitle,
                    Address = updatedSupplier.Address,
                    City = updatedSupplier.City,
                    Region = updatedSupplier.Region,
                    PostalCode = updatedSupplier.PostalCode,
                    Country = updatedSupplier.Country,
                    Phone = updatedSupplier.Phone,
                    Fax = updatedSupplier.Fax
                });

                return Request.CreateResponse(HttpStatusCode.OK, supplier);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        // DELETE api/Supplier/5
        [Authorize]
        [WebApiAuthentication]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (id == 0)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                var supplierToDelete = SupplierRepository.Get(id);
                if (supplierToDelete == null)
                    throw new NullReferenceException(string.Format("No Supplier with id {0} available.", id));

                SupplierRepository.Delete(id);
                Request.CreateResponse(HttpStatusCode.OK, string.Format("Supplier with id {0} deleted.", id));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}