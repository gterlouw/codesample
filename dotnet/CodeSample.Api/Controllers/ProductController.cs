﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using CodeSample.Api.Helpers;
using CodeSample.Api.Models;
using CodeSample.Api.Security.Attributes;
using CodeSample.Data.Domain;
using CodeSample.Data.Repositories;
using CodeSample.Data.Repositories.Interfaces;
using CodeSample.Business.Services;
using CodeSample.Business.Services.Interfaces;

namespace CodeSample.Api.Controllers
{
    public class ProductController : ApiController
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IProductRepository _productRepository;
        public IProductRepository ProductRepository
        {
            get { return _productRepository ?? (_productRepository = new ProductRepository()); }
        }

        private IEmailService _emailService;

        public IEmailService EmailService
        {
            get { return _emailService ?? (_emailService = new EmailService()); }
        }

        // GET api/product
        [HttpGet]
        public IEnumerable<Product> Get()
        {
            try
            {
                return ProductRepository.Get();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        // GET api/product/5
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var product = ProductRepository.Get(id);
                if (product == null)
                    return NotFound();

                return Ok(product);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return null;
            }
        }

        // POST api/product
        [HttpPost]
        [Authorize]
        [WebApiAuthentication]
        public HttpResponseMessage Post([FromBody]ApiCreateProduct newProduct)
        {
            try
            {
                if (!ModelState.IsValid || newProduct == null)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);


                var product = ProductRepository.Create(new Product {
                    Name = newProduct.Name,
                    SupplierId = newProduct.SupplierId,
                    CategoryId = newProduct.CategoryId,
                    QuantityPerUnit = newProduct.QuantityPerUnit,
                    UnitPrice = newProduct.UnitPrice,
                    UnitsInStock = newProduct.UnitsInStock,
                    UnitsOnOrder = newProduct.UnitsOnOrder,
                    ReorderLevel = newProduct.ReorderLevel,
                    Discontinued = newProduct.Discontinued
                });


                if (product == null)
                    return new HttpResponseMessage(HttpStatusCode.NotFound);

                var mailBody = RenderViewHelper.RenderViewToString("Empty", "~/Views/EmailTemplates/NewProduct.cshtml",
                    new NewProductEmailModel()
                    {
                        Title = ConfigurationManager.AppSettings["NewProductSubject"],
                        Body = "A new product is added to the catalog!",
                        Url = Url.Link("DefaultApi", new { controller = "Product", id = product.Id })
                    });
                EmailService.Send(ConfigurationManager.AppSettings["NewProductSubject"], mailBody,
                    "consumer@codesample.nl");

                return new HttpResponseMessage(HttpStatusCode.Created);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
            }
        }

        // PUT api/product/5
        [HttpPut]
        [Authorize]
        [WebApiAuthentication]
        public HttpResponseMessage Put([FromBody]ApiUpdateProduct updatedProduct)
        {
            try
            {
                if (!ModelState.IsValid || updatedProduct == null)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                var productToUpdate = ProductRepository.Get(updatedProduct.Id);
                if (productToUpdate == null)
                    throw new NullReferenceException(string.Format("No product with id {0} available.", updatedProduct.Id));

                var product = ProductRepository.Update(new Product {
                    Id = updatedProduct.Id,
                    Name = updatedProduct.Name,
                    SupplierId = updatedProduct.SupplierId,
                    CategoryId = updatedProduct.CategoryId,
                    QuantityPerUnit = updatedProduct.QuantityPerUnit,
                    UnitPrice = updatedProduct.UnitPrice,
                    UnitsInStock = updatedProduct.UnitsInStock,
                    UnitsOnOrder = updatedProduct.UnitsOnOrder,
                    ReorderLevel = updatedProduct.ReorderLevel,
                    Discontinued = updatedProduct.Discontinued
                });

                return Request.CreateResponse(HttpStatusCode.OK, product);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
            }
        }

        // DELETE api/product/5
        [Authorize]
        [WebApiAuthentication]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (id == 0)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                var productToDelete = ProductRepository.Get(id);
                if (productToDelete == null)
                    throw new NullReferenceException(string.Format("No product with id {0} available.", id));

                ProductRepository.Delete(id);
                return Request.CreateResponse(HttpStatusCode.OK, string.Format("Product with id {0} deleted.", id));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
            }
        }
    }
}
