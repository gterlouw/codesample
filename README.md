# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains a hobby project that allows me to quickly try some dotnet features.
I started with developing a WebApi application to fetch database from the Northwind database
I used Dapper as an ORM (which is new to me) and ASP.NET WebApi 2.

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

You can recreate the Northwind database using the SQL files in the repository.
Create a connectionstring for the database and set it in the connectionstrings.config file.
Restore the nuget packages used in the solution.
Build the solution.

I use the local.codesample.nl domain as my development url (You should not have to use it in your setup)

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact